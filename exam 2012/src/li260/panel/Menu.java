package li260.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import li260.ihm.observer.ButtonAction;
import li260.tools.S;

public class Menu extends JPanel implements ActionListener{
	
	private SpinnerNumberModel radarModel;
	private SpinnerNumberModel vampireModel;
	private SpinnerNumberModel angleModel;
	private JComboBox listeCircuits;
	private JButton valider;
	private JLabel pic = null;
	private boolean init = false;
	private Box hbox;
	private int optionW = 95;
	private int optionH = 50;
	private int apercuW = 300;
	private int validerH = 70;
	
	public Menu(){
		super();
//		this.setLayout(new BorderLayout());
		setVisible(true);	
		
//		Affichage des options
		JPanel radar = new JPanel();
		radar.setPreferredSize(new Dimension(optionW, optionH));
		radar.setLayout(new FlowLayout());
		radar.setVisible(true);
		JLabel radarTexte = new JLabel("Faisceaux");
		SpinnerNumberModel radarModel = new SpinnerNumberModel(20, 1, 100, 1); 
		JSpinner radarField = new JSpinner(radarModel);
		radar.add(radarTexte);
		radar.add(radarField);

		JPanel vampire = new JPanel();
		vampire.setPreferredSize(new Dimension(optionW, optionH));
		vampire.setLayout(new FlowLayout());
		vampire.setVisible(true);
		JLabel vampireTexte = new JLabel("Vampire");
		SpinnerNumberModel vampireModel = new SpinnerNumberModel(20, 1, 50, 1); 
		JSpinner vampireField = new JSpinner(vampireModel);
		vampire.add(vampireTexte);
		vampire.add(vampireField);

		JPanel angle = new JPanel();
		angle.setPreferredSize(new Dimension(optionW, optionH));
		angle.setLayout(new FlowLayout());
		angle.setVisible(true);
		JLabel angleTexte = new JLabel("limite angles");
		SpinnerNumberModel angleModel = new SpinnerNumberModel(4, 0, 32, 0.1); 
		JSpinner angleField = new JSpinner(angleModel);
		angle.add(angleTexte);
		angle.add(angleField);
		
		JPanel champs = new JPanel();
		champs.add(radar);
		champs.add(vampire);
		champs.add(angle);
		champs.setPreferredSize(new Dimension(3*optionW+20, optionH+20));
		
//		Liste déroulante
		listeCircuits = new JComboBox(nomCircuits());
		listeCircuits.addActionListener(this);
		listeCircuits.setPreferredSize(new Dimension(3*optionW, 20));
		listeCircuits.setMaximumRowCount(20);
		
		JPanel liste = new JPanel();
		liste.add(listeCircuits);
		liste.setPreferredSize(new Dimension(3*optionW+20, apercuW-optionH-20-validerH-20));

//		Bouton valider
		valider = new JButton("Valider");
		valider.addActionListener(this);
		valider.setPreferredSize(new Dimension(3*optionW, validerH));
		
		JPanel btn = new JPanel();
		btn.add(valider);
		btn.setPreferredSize(new Dimension(3*optionW+20, validerH));
		
		
//		Apercu
		pic = new JLabel();
		
		hbox = Box.createHorizontalBox();
		this.add(hbox);
		hbox.add(pic);
//		hbox.add(Box.createHorizontalGlue());
		Box vbox = Box.createVerticalBox();
		hbox.add(vbox);
//		Box hbox2 = Box.createHorizontalBox();
//		vbox.add(hbox2);
//		hbox2.add(radar);
//		hbox2.add(vampire);
//		hbox2.add(angle);
//		hbox2.add(champs);
		vbox.add(champs);
//		vbox.add(Box.createGlue());
//		vbox.add(listeCircuits);
		vbox.add(liste);
		vbox.add(Box.createVerticalGlue());
//		vbox.add(valider);
		vbox.add(btn);

		
//		Ajout des éléments dans l'interface
//		this.add(radar);
//		this.add(vampire);
//		this.add(angle);
//		this.add(listeCircuits);
//		this.add(pic);
//		this.add(valider);
		
		apercu();
		
	}

	public void apercu(){
		BufferedImage image = null;
		String chemin = "/home/orlin/LI260/"+listeCircuits.getSelectedItem()+".png";
		try {
			image = ImageIO.read(new File(chemin));
		} catch (IOException e) {
			e.printStackTrace();
		}
		double pan = apercuW;
		double img = image.getHeight(this);
		double res = pan/img;
		
		if(image.getWidth(this) >= image.getHeight(this))
			res = (apercuW*1.0) / (image.getWidth(this));
		else
			res = (apercuW*1.0) / (image.getHeight(this));
		
		image = scale(image, res);
		pic = new JLabel(new ImageIcon(image));
		hbox.remove(0);
		hbox.add(pic, 0);
		this.revalidate();
	}
	
	public String[] nomCircuits(){
		String[] liste = new String[16];
		
		for(int i=0 ; i<8 ; i++)
			liste[i] = (i+1)+"_safe";
		liste[8] = "aufeu";
		liste[9] = "bond_safe";
		liste[10] = "Een2";
		liste[11] = "labymod";
		liste[12] = "labyperso";
		liste[13] = "perso";
		liste[14] = "t2009";
		liste[15] = "t260_safe";
		
		return liste;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
		if(source == listeCircuits)
			apercu();
	}
	public static BufferedImage scale(BufferedImage bi, double scaleValue) {
		AffineTransform tx = new AffineTransform();
		tx.scale(scaleValue, scaleValue);
		AffineTransformOp op = new AffineTransformOp(tx,
				AffineTransformOp.TYPE_BILINEAR);
		BufferedImage biNew = new BufferedImage( (int) (bi.getWidth() * scaleValue),
				(int) (bi.getHeight() * scaleValue),
				bi.getType());
		return op.filter(bi, biNew);
                
	}
}
//		new Double(value), new Double(minimum), new Double(maximum), new Double(stepSize)

		//int fifty = modelRadar.getNumber().intValue(); 