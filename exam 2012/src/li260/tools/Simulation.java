package li260.tools;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import li260.Voiture.Commande;
import li260.Voiture.Voiture;
import li260.Voiture.VoitureException;
import li260.circuit.Circuit;
import li260.circuit.Terrain;
import li260.ihm.observer.UpdateEventListener;
import li260.ihm.observer.UpdateEventSender;
import li260.strategy.Strategy;


public class Simulation implements UpdateEventSender{
	private Voiture voiture;
	private Circuit track;
	private Strategy str; 
	private EtatSimulation state;
	private ArrayList<Commande> record;
	private BufferedImage im;

	private ArrayList<UpdateEventListener> list;


	public Simulation(Voiture v, Circuit track, Strategy s){
		super();
		voiture=v;
		this.track=track;
		this.str=s;
		list = new ArrayList<UpdateEventListener>();
		record= new ArrayList<Commande>();
	}
	
	public void play() throws VoitureException{
		state = state.Run;
		while(state==state.Run) {
			playOneShot();
		}
	}
	
	private void playOneShot()throws VoitureException{

		Commande c = new Commande(str.getCommande());
		record.add(c);
//		if(voiture.getVitesse()!=0.9)
//			S.out("Vitesse voiture : " + voiture.getVitesse());
		voiture.drive(c);
		update();
		updateState();
	}
	
	private void updateState(){
		if (!Tools.isRunnable(track.getTerrain(voiture.getPosition()))){
			S.err("Sortie :" + Tools.stringFromTerrain(track.getTerrain(voiture.getPosition())));
			S.err("Coordonnées : "+(int)voiture.getPosition().getX()+ " , " +(int)voiture.getPosition().getY());
			state=EtatSimulation.Echec;
			System.out.println("echec : votre voiture est sortie de la piste");
		}
		else if(/*track.getArrivees().contains(voiture.getPosition())*/  track.getTerrain(voiture.getPosition())==Terrain.EndLine){
			System.err.println("arrivee");
			if (voiture.arriveeBonSens(track)){
				state=EtatSimulation.Succes;
				System.out.println("succes : la voiture est bien arrivee");
			}
			else{
				state=EtatSimulation.Echec;
				System.out.println("echec : voiture arrivee dans le mauvais sens");
			}	
		}
		else state=EtatSimulation.Run;
	}
	public ArrayList<Commande> getRecord(){
		return record;
	}

	public void update() {
		for(UpdateEventListener listener: list)	
			listener.manageUpdate();			
	}
	public void add(UpdateEventListener l) {
		list.add(l);
	}

}
