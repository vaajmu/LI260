package li260.ihm.observer;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;

import li260.tools.S;

public class ButtonAction extends AbstractAction {

	JFrame fenetre;
	
	public ButtonAction(String texte, JFrame fenetre){
		super(texte);
		this.fenetre = fenetre;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		S.out("Dimensions : " + fenetre.getSize().getWidth() + " , " + fenetre.getSize().getHeight());

	}

}
