package li260.strategy;

import li260.Voiture.Commande;

public interface Strategy {
	public Commande getCommande();
}
