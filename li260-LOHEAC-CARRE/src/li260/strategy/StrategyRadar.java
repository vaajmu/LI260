package li260.strategy;

import li260.Voiture.Commande;
import li260.Voiture.Voiture;
import li260.radars.Radar;

public class StrategyRadar implements Strategy {

	private Radar radar;
	private int index;
	private double [] thetas;
	private Commande [] coms;
	private Voiture voiture;

	public StrategyRadar(Radar r,Voiture v){
		this.radar = r;
		index = 0;
		thetas = radar.getThetas();
		coms = radar.getAllCom();
		voiture =v;
	}

	public Commande getCommande() {
		
		double [] scores = radar.scores();
		int index = radar.getBestIndex();
		Commande c = coms[index];
		double turnAbs = Math.min( Math.abs(c.getTurn()), voiture.getMaxTurnSansDerapage());
		return new Commande(c.getAcc(), turnAbs * Math.signum(c.getTurn()));

	}

}
