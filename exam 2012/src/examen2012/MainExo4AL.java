//package examen2012;
//
//import java.awt.Dimension;
//import java.io.File;
//import java.io.IOException;
//import javax.imageio.ImageIO;
//import javax.swing.JFrame;
//
//import li260.Voiture.Commande;
//import li260.Voiture.FerrariFactory;
//import li260.Voiture.Voiture;
//import li260.Voiture.VoitureException;
//import li260.circuit.Circuit;
//import li260.circuit.CircuitFactory;
//import li260.ihm.observer.CircuitObserver;
//import li260.ihm.observer.Controler;
//import li260.ihm.observer.IHMSwing;
//import li260.ihm.observer.TrajetObserver;
//import li260.ihm.observer.VoitureObserver;
//import li260.selector.SelectorGhost;
//import li260.selector.SelectorLigneArr;
//import li260.selector.SelectorRadarDijkstra;
//import li260.selector.SelectorVMin;
//import li260.strategy.Dijkstra;
//import li260.strategy.Radar;
//import li260.strategy.RadarDijkstra;
//import li260.strategy.StrategyLigneArr;
//import li260.strategy.StrategyRadar;
//import li260.strategy.StrategySafe;
//import li260.strategy.StrategySelector;
//import li260.strategy.StrategyVMin;
//import li260.tools.ImTools;
//import li260.tools.Simulation;
//
//
//
//
//public class MainExo4AL {
//
//	public static double[] thetas = null;
//	public static Commande[] allCom = null;
//	public static String nomRendu = "formule1";
//	public static String nomFichier = "formule1";
//	public static String nomCommande = "formule1";
//	
//
//	public static void main(String[] args) throws VoitureException{	
////definition commandes
//		thetas = definirThetas(20);
//		allCom = definirCommandes(thetas);
//// choix circuit
//		String nom_circuit = nomFichier;
//
//// Construction du circuit
//		CircuitFactory cf = new CircuitFactory("../circuitsTrk/"+nom_circuit+".trk");
//		Circuit c = cf.build();
//// Construction de la voiture
//		FerrariFactory f = new FerrariFactory(c);
//		Voiture v =f.build();
//// Construction du radar
//		Dijkstra dijk = new Dijkstra(c);
//		Radar rd = new RadarDijkstra(v, c, dijk, thetas, allCom);
//
//// Creation des strategies
//		StrategySelector stSelector = new StrategySelector();
//		StrategyLigneArr stLigArr = new StrategyLigneArr(c, v);
//		StrategyRadar stRadarDijk = new StrategyRadar(rd,v);
//		StrategySafe stSafe = new StrategySafe(stRadarDijk);
//		StrategyVMin stV = new StrategyVMin(stRadarDijk, v);
//// Creation des selectors
//		//SelectorSafe seSafe = new SelectorSafe(c,v);
//		SelectorLigneArr seLigArr = new SelectorLigneArr(c, v);
//		SelectorRadarDijkstra seRadarDijk = new SelectorRadarDijkstra();
//		SelectorVMin seV = new SelectorVMin(v);
//		SelectorGhost seGhost = new SelectorGhost(v, c);
//// Ajout des stratégies et des selectors
//		//stSelector.add(stSafe, seSafe);
//		stSelector.add(stV, seV);
//		stSelector.add(stSafe, seGhost);
//		stSelector.add(stLigArr, seLigArr);
//		stSelector.add(stRadarDijk, seRadarDijk);
//
//// Creation Simulation 
//		Simulation sim = new Simulation(v, c, stSelector);
//
////IHM
//		Controler ihm = new Controler(c);	
//		IHMSwing ihmSwing = new IHMSwing();
//
//// Creation fenetre
//		java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
//		JFrame fenetre = new JFrame("Simulation LI260 !");	
//		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		fenetre.setPreferredSize(new Dimension(c.getHeight()+10, c.getWidth()+30));
//		fenetre.setLocation(
//				(screenSize.width-fenetre.getWidth()),
//				(0)
//				);
//
//// Ajout des observeurs	
//		ihmSwing.add(new VoitureObserver(v));
//		ihmSwing.add(new CircuitObserver(c));
//		ihmSwing.add(new TrajetObserver(v));	
//
//// Ajout des recepteurs dans l emetteur	
//		sim.add(ihmSwing); 
//		dijk.add(ihmSwing);
//
//// Dijk Compute
//		dijk.compute();
//
//// Modification fenetre
//		ihmSwing.setPreferredSize(new Dimension(768, 1024));	
//		fenetre.setContentPane(ihmSwing);	
//		//fenetre.add(panel);
//		fenetre.setVisible(true);
//		fenetre.pack();
//
//// PLAY
//		sim.play();
//
//		//TrackPanel panel = new TrackPanel();	
//
//
//
//		try{
//			ImageIO.write(ihm.getImage(), "png", new File("../exam-LOHEAC-Antoine-Sujet2/"+nomRendu+".png"));
//		}catch (IOException e) {
//			System.out.println("error");
//			e.printStackTrace();
//		}
//
//
//		/*
//		JFrame fen2 = new JFrame();	
//		fen2.add(new JLabel(new ImageIcon(ihm.getImage())));	
//		fen2.setVisible(true);	
//		fen2.pack();
//		//*/
//		/*
//		TrackPanel panel = new TrackPanel();	
//		JFrame fenetre = new JFrame("Simulation LI260 !");	
//		fenetre.add(panel);
//		fenetre.setVisible(true);	
//		fenetre.pack();
//		//*/
//		
//// Sauvegarde des commandes
//		ImTools.saveListeCommande(sim.getRecord(), "../exam-LOHEAC-Antoine-Sujet2/"+nomCommande+".com");
//		System.out.println("commandes enregistrees");
//
//	}
////
//	public static double[] definirThetas(double[] angles){
//		int i;
//		double[] thetas = new double[2*angles.length];
//
//		for(i=0; i<angles.length; i++){
//			thetas[i]=angles[i];
//			thetas[i+angles.length]=(-1)*(angles[i]);
//		}
//		return thetas;
//	}
//
//	public static Commande[] definirCommandes(Commande[] com){
//		int i;
//		Commande[] allCom = new Commande[2*com.length];
//		for(i=0; i<com.length; i++){
//			allCom[i]=com[i];
//			allCom[i+com.length]=new Commande(com[i]);
//			allCom[i+com.length].setTurn(-1*com[i].getTurn());
//		}
//		return allCom;
//	}
//
//	public static double[] definirThetas(int n){
//		int i;
//		double[] thetas = new double[2*n+1];
//
//		thetas[0] = 0;
//		for(i=1;i<=n;i++){
//			thetas[i] = (i*Math.PI)/(2*n);
//			thetas[n+i] = -(i*Math.PI)/(2*n);
//		}
//		return thetas;
//	}
//
//	public static Commande[] definirCommandes(double[] thetas){
//		int i;
//		Commande[] com = new Commande[thetas.length];
//		for(i=0;i<thetas.length;i++){
//			if(Math.abs(thetas[i]) == 0){
//				com[i] = new Commande(1, 0);
//			}
//			else if(Math.abs(thetas[i]) < 0.2){
//				com[i] = new Commande(1, thetas[i]/0.2);
//			}
//			else{
//				com[i] = new Commande(1, 1*Math.signum(thetas[i]));
//			}
//			if(Math.abs(thetas[i])>=Math.PI/5)
//				com[i].setAcc(-1);
//		}
//		return com;
//	}
//}