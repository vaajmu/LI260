package li260.ihm.observer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import li260.Voiture.Voiture;

public class VoitureObserver implements Observer,SwingObserver{

	private Voiture voiture;	
	private Color color = Color.yellow; 	
	private Color colorDerape = Color.red;	
	
	public VoitureObserver(Voiture voiture) { 	
		this.voiture = voiture;
	}	
	
	public int getX(){
		return (int) voiture.getPosition().getX();
	}	
	public int getY(){
		return (int) voiture.getPosition().getY();
	}	
	public Color getColor() {	
		if(voiture.getDerapage()) 
			return colorDerape;	
		return color;	
	}	
	public void print(BufferedImage im) {	
		im.setRGB(getX(), getY(), getColor().getRGB());	
	}

	public void paint(Graphics g) {
		g.setColor(getColor());
		g.drawLine(getX(), getY(), getX(), getY());
	}

}
