package li260.tools;

public class Commande {
	private double acceleration;
	private double rotation;
	
	public Commande(double acceleration, double rotation) {
		super();
		this.acceleration = acceleration;
		this.rotation = rotation;
	}
	
	public double getAcc() {
		return acceleration;
	}

	public double getTurn() {
		return rotation;
	}
	
	
	public void setAcc(double acceleration) {
		this.acceleration = acceleration;
	}

	public void setTurn(double rotation) {
		this.rotation = rotation;
	}

	@Override
	public String toString() {
		return "Commande [acceleration=" + acceleration + ", rotation="
				+ rotation + "]";
	}
	

}
