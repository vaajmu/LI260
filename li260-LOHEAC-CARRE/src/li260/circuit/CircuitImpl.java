package li260.circuit;

import java.util.ArrayList;

import li260.geometry.Vecteur;
import li260.geometry.Vtools;
import li260.tools.S;
import li260.tools.Tools;

public class CircuitImpl implements CircuitModifiable{

	private Terrain[][] t;
	private Vecteur pointDepart;
	private Vecteur directionDepart;
	private Vecteur directionArrivee;
	private ArrayList<Vecteur> arrivees;

	public CircuitImpl(Terrain[][] t, Vecteur pointDepart, Vecteur directionDepart, Vecteur directionArrivee,ArrayList<Vecteur> arrivees ){
		this.t = t;
		this.pointDepart = pointDepart;
		this.directionDepart = directionDepart;
		this.directionArrivee = directionArrivee;
		this.arrivees = arrivees;
	}

	public Terrain getTerrain(int i, int j) {
		try{
			return t[i][j];
		}catch(ArrayIndexOutOfBoundsException e){
			S.err("Dépassement de bornes");
			return Terrain.Herbe;
		}
	}

	public Terrain getTerrain(Vecteur p) {
		try{
			return t[(int)p.getX()][(int) p.getY()];
		}catch(ArrayIndexOutOfBoundsException e){
			S.err("Dépassement de bornes");
			return Terrain.Herbe;
		}
	}
	
	public void setTerrain(int i, int j, Terrain t) {
		this.t[i][j]=t;
	}

	public Vecteur getPointDepart() {
		return pointDepart;
	}

	public Vecteur getDirectionDepart() {
		return directionDepart;
	}

	public Vecteur getDirectionArrivee() {
		return directionArrivee;
	}

	public int getWidth() {

		return t[0].length;
	}

	public int getHeight() {
		return t.length;
	}

	public ArrayList<Vecteur> getArrivees() {
		return arrivees;
	}

	public boolean isVisible(Vecteur position, Vecteur but){
		double epsilon = 0.1;
		Vecteur pos = Vtools.clonage(position);
		Vecteur dir = new Vecteur(pos, but);
		dir = Vtools.normaliser(dir);
		while (Tools.isRunnable(this.getTerrain(pos))){
			if(this.getTerrain(pos)==Terrain.EndLine){
				return true;
			}
			pos = Vtools.addition(pos, Vtools.fact(dir, epsilon));
		}
		return false;
	}
}
