package li260.ihm.observer;

import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

import li260.ihm.observer.UpdateEventListener;

public class IHMSwing extends JPanel implements UpdateEventListener{
	
	private ArrayList <SwingObserver> list;
	
	public IHMSwing(){
		super();
		list = new ArrayList<SwingObserver>();
	}
	
	public void add(SwingObserver o){
		list.add(o);
	}
	
	public void manageUpdate() {	
		paintImmediately(getBounds());	
	}
	public void paint(Graphics g){	
		super.paint(g);	
		for(SwingObserver o:list)	
			o.paint(g);	
	}

}
