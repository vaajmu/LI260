package li260.strategy;

import li260.Voiture.Commande;
import li260.Voiture.Voiture;
import li260.circuit.Circuit;
import li260.circuit.Terrain;
import li260.geometry.Vecteur;
import li260.geometry.Vtools;
import li260.tools.Tools;

public class RadarDijkstra extends RadarClassique{
	
	private Dijkstra dijk;
	
	public RadarDijkstra(Voiture voiture, Circuit circuit, Dijkstra dijk, double[] thetas, Commande[] allCom){
		super(voiture, circuit, thetas, allCom);
		this.dijk = dijk;
		
	}
	
	public double calcScore(double theta){
		Vecteur pos = Vtools.clonage(voiture.getPosition());
		Vecteur dir = Vtools.clonage(voiture.getDirection());
		dir.rotation(theta);
		double best = Integer.MAX_VALUE;
		while (Tools.isRunnable(track.getTerrain(pos)) /*&& ((track.getTerrain(pos)==Terrain.EndLine && Vtools.prodScal(dir,track.getDirectionArrivee())>=0) || (track.getTerrain(pos)!=Terrain.EndLine)*/){
			if(track.getTerrain(pos) == Terrain.EndLine && Vtools.prodScal(dir,track.getDirectionArrivee())<0 ) 
				return Integer.MIN_VALUE;
			pos = Vtools.addition(pos, Vtools.fact(dir,epsilon));
			if( dijk.getDist( (int)pos.getX(), (int)pos.getY() ) < best){
				best = dijk.getDist( (int)pos.getX(), (int)pos.getY());
			}
			
		}
		return -best;
	}
}
