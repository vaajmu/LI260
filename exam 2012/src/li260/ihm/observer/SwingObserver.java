package li260.ihm.observer;

import java.awt.Graphics;

public interface SwingObserver { 
	
	public void paint(Graphics g);

}
