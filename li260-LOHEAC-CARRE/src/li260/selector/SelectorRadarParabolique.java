package li260.selector;

import li260.radars.Radar;
import li260.tools.S;

public class SelectorRadarParabolique implements Selector {

	Radar rP;
	Radar rD;
	public SelectorRadarParabolique(Radar rP, Radar rD){
		this.rP = rP;
		this.rD = rD;
	}
	
	public boolean isSelected() {
		// Recherche du meilleur score du radar parabolique
		double [] scoresRP = rP.scores();
		double bestRP = scoresRP[rP.getBestIndex()];
		// Recherche du meilleur score du radar dijkstra
		double [] scoresRD = rD.scores();
		double bestRD = scoresRD[rD.getBestIndex()];
		// Rend vrai si RP est meilleur
		if(bestRP < bestRD){
			S.out("Radar parabolique");
			return true;
		}
		// Rend non sinon
		return false;
	}

}
