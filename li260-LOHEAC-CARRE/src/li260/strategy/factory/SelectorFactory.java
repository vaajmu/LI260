package li260.strategy.factory;

import li260.Voiture.Commande;
import li260.Voiture.Voiture;
import li260.circuit.Circuit;
import li260.radars.Radar;
import li260.radars.RadarClassique;
import li260.radars.RadarDijkstra;
import li260.radars.RadarParabolique;
import li260.selector.SelectorCube;
import li260.selector.SelectorGhost;
import li260.selector.SelectorLigneArr;
import li260.selector.SelectorRadarDijkstra;
import li260.selector.SelectorRadarParabolique;
import li260.selector.SelectorTestAngles;
import li260.selector.SelectorVMin;
import li260.strategy.Strategy;
import li260.strategy.StrategyLigneArr;
import li260.strategy.StrategyRadar;
import li260.strategy.StrategyRadarParabolique;
import li260.strategy.StrategySafe;
import li260.strategy.StrategySelector;
import li260.strategy.StrategyTestAngles;
import li260.strategy.StrategyVMin;
import li260.strategy.dijkstra.Dijkstra;
import li260.tools.Faisceaux;

public class SelectorFactory {

	StrategySelector stSelector;
	Dijkstra dijk;

	public SelectorFactory(Voiture v, Circuit c, Faisceaux f, double[] vM, Commande[] aC){

		// Construction du radar
		dijk = new Dijkstra(c);
		dijk.compute();
		Radar rd = new RadarDijkstra(v, c, dijk, f.getThetas(), f.getAllCom());
		RadarClassique rC = new RadarClassique(v, c, f.getThetas(), f.getAllCom());
		Radar rP = new RadarParabolique(v, c, dijk, vM, aC);
		
		// Creation des strategies
		stSelector = new StrategySelector();
		StrategyRadar stRC = new StrategyRadar(rC, v);
		StrategyLigneArr stLigArr = new StrategyLigneArr(c, v);
		StrategyRadar stRadarDijk = new StrategyRadar(rd,v);
		StrategySafe stSafe = new StrategySafe(stRadarDijk);
		StrategyVMin stV = new StrategyVMin(stRadarDijk, v);
//		StrategyVMin stV = new StrategyVMin(stRC, v);
		StrategyTestAngles stTA = new StrategyTestAngles(v);
		StrategyRadarParabolique stRP = new StrategyRadarParabolique(rP, v, c);
		
		// Creation des selectors
		SelectorLigneArr seLigArr = new SelectorLigneArr(c, v);
		SelectorRadarDijkstra seRadarDijk = new SelectorRadarDijkstra();
		SelectorVMin seV = new SelectorVMin(v);
		SelectorGhost seGhost = new SelectorGhost(v, c, f);
		SelectorTestAngles seTA = new SelectorTestAngles();
		SelectorRadarParabolique seRP = new SelectorRadarParabolique(rP, rd);
		SelectorCube seC = new SelectorCube(dijk, v);
		
		// Ajout des stratégies et des selectors
		
		// Strat�gie Test angles
//		stSelector.add(stTA, seTA);
		
		// Strat�gie Vitesse minimum
		stSelector.add(stV, seV);
		
		// Strat�gie ghost
//		stSelector.add(stSafe, seGhost);
		
		// Strat�gie cube
		stSelector.add(stSafe, seC);
		
		// Strat�gie Ligne arriv�e
		stSelector.add(stLigArr, seLigArr);
		
		// Strat�gie radar parabolique
//		stSelector.add(stRP, seRP);
		
		// Strat�gie radar Dijkstra
		stSelector.add(stRadarDijk, seRadarDijk);
		
		// Strat�gie radar classique
//		stSelector.add(stRC, seRadarDijk);
	}

	public StrategySelector getStrategy(){
		return stSelector;
	}
	public Dijkstra getDijk(){
		return dijk;
	}


}
