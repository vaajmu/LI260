package li260.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import li260.main.Controleur;
import li260.tools.Coord;
import li260.tools.S;

public class Menu extends JPanel{
	private JLabel im = null;
	private JComboBox liste;
	private JButton valider;
	private JButton rafraichir;
	
	private JFrame fen;
	private int size = 300;
	private int heightNS = 40;
	private int widthEW = 60;
	
	public Menu(JFrame fen){
		
		this.fen = fen;
		
		this.setLayout(new BorderLayout());
		this.setPreferredSize(Coord.getDimension(fen.getWidth(), fen.getHeight()));
		
		liste = new JComboBox(nomCircuits());
		liste.setEditable(true);
		valider = new JButton("Valider");
		rafraichir = new JButton("rafraichir");
		
		heightNS = (int)valider.getPreferredSize().getHeight();
		widthEW = (int)valider.getPreferredSize().getWidth();
		
		im = new JLabel();
		rafraichirIm();
		
		this.add(liste, BorderLayout.NORTH);
		this.add(im, BorderLayout.CENTER);
		this.add(valider, BorderLayout.EAST);
		this.add(rafraichir, BorderLayout.SOUTH);
	}
	public void addActionListener(Controleur controleur){
		
		liste.addActionListener(controleur);
		valider.addActionListener(controleur);
		rafraichir.addActionListener(controleur);
	}
	public void rafraichirIm(){
		
		int width = fen.getWidth();
		int height = fen.getHeight()-78;
		
		if(width < height)
			this.size = width;
		else
			this.size = height;
		
		BufferedImage image = null;
		String chemin = "../circuitsImages/"+liste.getSelectedItem()+".png";
		try {
			image = ImageIO.read(new File(chemin));
		}catch (IIOException e) {
			this.remove(im);
			im = new JLabel("Pas d'image disponible pour ce circuit");
			this.revalidate();
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		double pan = size;
		double img = image.getHeight(this);
		double res = pan/img;
		
		if(image.getWidth(this) >= image.getHeight(this))
			res = (size*1.0) / (image.getWidth(this));
		else
			res = (size*1.0) / (image.getHeight(this));
		
		image = scale(image, res);
		this.remove(im);
		im = new JLabel(new ImageIcon(image));
		this.add(im, BorderLayout.CENTER);
		this.revalidate();
	}
	public String[] nomCircuits(){
		String[] liste = new String[16];
		
		for(int i=0 ; i<8 ; i++)
			liste[i] = (i+1)+"_safe";
		liste[8] = "aufeu";
		liste[9] = "bond_safe";
		liste[10] = "Een2";
		liste[11] = "labymod";
		liste[12] = "labyperso";
		liste[13] = "perso";
		liste[14] = "t2009";
		liste[15] = "t260_safe";
		
		return liste;
	}
	public static BufferedImage scale(BufferedImage bi, double scaleValue) {
		AffineTransform tx = new AffineTransform();
		tx.scale(scaleValue, scaleValue);
		AffineTransformOp op = new AffineTransformOp(tx,
				AffineTransformOp.TYPE_BILINEAR);
		BufferedImage biNew = new BufferedImage( (int) (bi.getWidth() * scaleValue),
				(int) (bi.getHeight() * scaleValue),
				bi.getType());
		return op.filter(bi, biNew);
                
	}
	public JButton getValider(){
		return valider;
	}
	public JButton getRafraichir(){
		return rafraichir;
	}
	public JComboBox getListe(){
		return liste;
	}
	public void setSize(Dimension fenSize){
	}
}