package li260.selector;


import li260.Voiture.Voiture;
import li260.circuit.Circuit;
import li260.strategy.dijkstra.Dijkstra;
import li260.strategy.factory.GhostFactory;
import li260.strategy.Strategy;
import li260.tools.Faisceaux;
import li260.tools.MiniSimu;
import li260.tools.S;

public class SelectorGhost implements Selector{

	private Voiture v;
	private Circuit circuit;
	private Faisceaux f;
	private Dijkstra dijk;
	private int nbCoups = 35;

	public SelectorGhost(Voiture v, Circuit c, Faisceaux f){
		this.v = v;
		this.f = f;
		circuit = c;

		dijk = new Dijkstra(c);
		dijk.compute();
	}

	public boolean isSelected(){
		Voiture fantome = v.clone();
		GhostFactory strfact = new GhostFactory(circuit, fantome, dijk, f);
		Strategy str = strfact.build();
		MiniSimu simu = new MiniSimu(str, fantome, circuit, nbCoups);
		if(!simu.play()){
			return false;
		}
		S.out("Ghost");
		return true;
	}
}
