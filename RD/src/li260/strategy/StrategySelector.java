package li260.strategy;

import java.util.ArrayList;

import li260.radar.Radar;
import li260.selector.Selector;
import li260.tools.Commande;

public class StrategySelector implements Strategy{// C’est une stratégie!	
	private ArrayList<Strategy> liStrategy; // Tout abstrait !	
	private ArrayList<Selector> liSelect;
	public StrategySelector(){	
		liStrategy = new ArrayList<Strategy>();	
		liSelect = new ArrayList<Selector>();	
	}	
	// Plutot sécurisé: on est sur qu'il y a concordance	
	public void add(Strategy str, Selector select){	
		liStrategy.add(str); liSelect.add(select);	
	}
	
	public Radar getRadar(){
		for(int i=0; i<liStrategy.size(); i++){	
			if(liSelect.get(i).isSelected())	
				return liStrategy.get(i).getRadar();	
			
		}
		
	}
	
	public Commande getCommande() {	
		for(int i=0; i<liStrategy.size(); i++){	
			if(liSelect.get(i).isSelected())	
				return liStrategy.get(i).getCommande();	
		}	
		// normalement on ne passe pas ici...	
		return liStrategy.get(liStrategy.size()-1).getCommande();	
	}	
}
