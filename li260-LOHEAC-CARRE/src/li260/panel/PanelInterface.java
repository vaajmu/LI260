package li260.panel;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import li260.ihm.observer.IHMSwing;
import li260.main.Controleur;
import li260.tools.Coord;

public class PanelInterface extends JPanel {

	private JFrame fen;
	private OptionsSimu options;
	private IHMSwing ihmSwing;
	
	public PanelInterface(JFrame fen, IHMSwing ihmSwing, int larg){
		
		this.fen = fen;
		this.ihmSwing = ihmSwing;

		this.setLayout(new BorderLayout());
		this.setPreferredSize(Coord.getDimension(fen.getWidth(), fen.getHeight()));
		
		options = new OptionsSimu(larg, fen.getHeight());
		
		this.add(ihmSwing, BorderLayout.CENTER);
		this.add(options, BorderLayout.EAST);
	}

	public void addActionListener(Controleur controleur){
		options.addActionListener(controleur);
	}
	public JButton getValider(){
		return options.getValider();
	}
	public void refresh(){
		this.revalidate();
	}
}
