package li260.circuit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import li260.geometry.NbColException;
import li260.geometry.NbLigException;
import li260.geometry.Vecteur;
import li260.tools.Tools;

public class CircuitFactory {

	String fichier;

	public CircuitFactory(String fichier){
		this.fichier = fichier;
	}

	public void supprDiagonalesImpossibles(CircuitModifiable cm){

		//    H .         H .
		//    . H    ->   H H
		
		//    . H         . H
		//    H .    ->   H H
		
		for(int i=1;i<cm.getHeight() ; i++){
			for(int j=1;j<cm.getWidth() ; j++){
					//premiere diagonale du carre
					if(!Tools.isRunnable(cm.getTerrain(i, j))
							&&!Tools.isRunnable(cm.getTerrain(i-1,j-1))
							&&Tools.isRunnable(cm.getTerrain(i-1, j))
							&&Tools.isRunnable(cm.getTerrain(i, j-1))){
						cm.setTerrain(i, j-1, Terrain.Obstacle);
					}
					//test autre diagonale
					if(!Tools.isRunnable(cm.getTerrain(i-1, j))
							&&!Tools.isRunnable(cm.getTerrain(i,j-1))
							&&Tools.isRunnable(cm.getTerrain(i, j))
							&&Tools.isRunnable(cm.getTerrain(i-1, j-1))){
						cm.setTerrain(i, j, Terrain.Obstacle);
					}
				}

			}	
		}

	public CircuitModifiable build() {
		CircuitModifiable cm=null;
		String line;
		char car;
		int nbCol;
		int nbLig;
		Terrain [][] t= null;
		Vecteur ptdep = null;
		Vecteur dirdep= null;
		Vecteur dirarr= null;
		ArrayList<Vecteur> arrivees = new ArrayList<Vecteur>();
		try {	
			FileReader fr = new FileReader(new File(fichier));	
			BufferedReader in = new BufferedReader(fr);	

			line = in.readLine();
			nbCol = Integer.parseInt(line);
			line = in.readLine();
			nbLig = Integer.parseInt(line);

			t = new Terrain[nbLig][nbCol];

			for(int i=0;i<nbLig ; i++){
				line = in.readLine();
				if(line==null){
					throw new NbLigException("Ligne "+(i+1)+" manquante, circuit incomplet");
				}
				for(int j=0;j<nbCol ;j++){
					car = line.charAt(j);
					if(car==-1){
						throw new NbColException("Ligne "+(i+1)+ " incomplete\nCircuit incomplet");
					}

					t[i][j] = Tools.terrainFromChar(car);
					if (t[i][j] == Terrain.StartPoint){
						ptdep = new Vecteur(i,j);						
					}
					if (t[i][j] == Terrain.EndLine){
						arrivees.add(new Vecteur(i,j));
					}
				}
			}	
			dirdep= new Vecteur(0,1);
			dirarr = new Vecteur(0,1);
			cm=new CircuitImpl(t,ptdep,dirdep,dirarr,arrivees);
			supprDiagonalesImpossibles(cm);
			in.close();	
		}
		catch (FileNotFoundException e) {	
			System.err.println("Unable to find "+ fichier);	
			e.printStackTrace();
		}	
		catch (IOException e) {	
			System.err.println("Can't open file " + fichier	+ " for reading... Loading aborted");
			e.printStackTrace();
		} 
		catch (NbColException e){
			System.err.println(e.toString());
			e.printStackTrace();
			return null;
		}
		catch (NbLigException e){
			System.err.println(e.toString());
			e.printStackTrace();
			return null;
		}
		catch (Exception e) {	
			System.err.println ("Invalid Format : " + fichier + "... Loading aborted");
			e.printStackTrace();
		}
		return cm;
	}

}
