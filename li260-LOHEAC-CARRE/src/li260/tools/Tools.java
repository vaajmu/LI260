package li260.tools;

import java.awt.Color;

import li260.circuit.Terrain;


public class Tools {

	public static boolean isRunnable(Terrain t){
		if(t==Terrain.Herbe || t==Terrain.Eau || t==Terrain.Obstacle)
			return false;
		else
			return true;
	}

	public static Terrain terrainFromChar(char c){
		switch(c){
		case 'g':
			return Terrain.Herbe;
		case 'b':
			return Terrain.Eau;
		case '.':
			return Terrain.Route;
		case 'w':
			return Terrain.BandeBlanche;
		case 'r':
			return Terrain.BandeRouge;
		case '*':
			return Terrain.StartPoint;
		case '!':
			return Terrain.EndLine;
		default :
			return null;
		}
	}
	static int colorFromTerrain(Terrain t){
		switch(t){
		case Herbe:
			return Color.green.getRGB();
		case Eau:
			return Color.blue.getRGB() ;
		case Route:
			return Color.gray.getRGB();
		case BandeBlanche:
			return Color.white.getRGB();
		case BandeRouge:
			return Color.red.getRGB();
		case StartPoint:
			return Color.cyan.getRGB();
		case EndLine:
			return Color.cyan.getRGB();
		case Obstacle :
			return Color.DARK_GRAY.getRGB();
		default :
			return 0;
		}
	}

	public static String stringFromTerrain(Terrain t){
		switch(t){
		case Herbe:
			return "Herbe";
		case Eau:
			return "Eau" ;
		case Route:
			return "Route";
		case BandeBlanche:
			return "BandeBlanche";
		case BandeRouge:
			return "BandeRouge";
		case StartPoint:
			return "StartPoint";
		case EndLine:
			return "EndLine";
		case Obstacle :
			return "Obstacle";
		default :
			return "";
		}
	}
	


}
