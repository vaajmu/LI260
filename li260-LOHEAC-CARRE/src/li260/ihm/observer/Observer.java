/**
 * 
 */
package li260.ihm.observer;

import java.awt.image.BufferedImage;

public interface Observer {
	
	public void print(BufferedImage im);
	
}
