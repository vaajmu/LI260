package li260.ihm.observer;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import li260.circuit.Circuit;
import li260.tools.ImTools;


public class Controler implements UpdateEventListener{	

	private ArrayList<Observer> list;	
	private BufferedImage image;	

	public Controler(Circuit c){
		IHMImage(c);
	}

	public void IHMImage(Circuit track){ // INITIALISATION	
		list = new ArrayList<Observer>();	
		image = ImTools.imageFromCircuit(track);	
	}	
	public void manageUpdate() { // MISE A JOUR	
		for(Observer o:list)	
			o.print(image);	
	}	
	public BufferedImage getImage(){
		return image;
	}	
	public void add(Observer obj) {
		list.add(obj);
	}
	
}

