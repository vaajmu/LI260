package li260.geometry;

public class NbLigException extends Exception{
	public NbLigException(String message){
		super(message);
	}
}