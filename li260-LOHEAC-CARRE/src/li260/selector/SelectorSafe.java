package li260.selector;

import li260.Voiture.Voiture;
import li260.circuit.Circuit;
import li260.geometry.Vecteur;
import li260.geometry.Vtools;
import li260.tools.Tools;

public class SelectorSafe implements Selector{

	Circuit c;
	Voiture v;

	public SelectorSafe(Circuit c, Voiture v){
		this.c = c;
		this.v = v;
	}

	public boolean isSelected() {
		double epsilon = 0.01;
		Vecteur dir = Vtools.clonage(v.getDirection());
		
		dir.rotation(v.getComPrec().getTurn());
		
		Vecteur pos = Vtools.clonage(v.getPosition());
		for(int i=0; i<8000; i++){
			pos = Vtools.addition(pos, Vtools.fact(dir,epsilon));
			if(!Tools.isRunnable(c.getTerrain(pos))){
				System.out.println("Safe");
				return true;
			}
		}
		return false;
	}
}