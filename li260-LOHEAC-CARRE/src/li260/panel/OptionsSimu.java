package li260.panel;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

import li260.main.Controleur;
import li260.tools.Coord;

public class OptionsSimu extends JPanel{
	
	private JButton valider;
	
	public OptionsSimu(int width, int height){

		Border raisedetched = BorderFactory.createEtchedBorder(EtchedBorder.RAISED);
		this.setLayout(new BorderLayout());
		this.setPreferredSize(Coord.getDimension(width, height));
		this.setBorder(raisedetched);
		
		valider = new JButton("Jouer");
		
		this.add(valider, BorderLayout.SOUTH);
	}

	public void addActionListener(Controleur controleur){
		valider.addActionListener(controleur);
	}
	public JButton getValider(){
		return valider;
	}
}
