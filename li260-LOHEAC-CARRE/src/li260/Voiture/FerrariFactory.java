package li260.Voiture;

import li260.circuit.Circuit;
import li260.geometry.Vecteur;


public class FerrariFactory implements VoitureFactory{

	private  double vmax = 0.9;
	private double alpha_c = 0.02;
	private double braquage = 0.2;
	private  double alpha_f= 0.001;
	private double beta_f= 0.002;
	private double alpha_derapage = 0.01;
	private double masse = 1;
	private double vitesse_sortie_derapage = 0.6;

	private Vecteur position;
	private Vecteur direction;

	private double vitesse = 0.;
	private double[] tabVitesse 	= {0.1,	0.2, 0.3, 0.4, 0.5,	0.6, 0.7, 0.8,	0.9, 1.}; 
	private double[] tabTurn 	= {1., 1., 0.8, 0.7, 0.6, 0.4,	0.3,0.2, 0.1, 0.075};

	public FerrariFactory(Circuit c){
		super();	
		position = c.getPointDepart();	
		direction = c.getDirectionDepart();
	}
	
	public Voiture build(){
		return new VoitureImpl(vmax, braquage, alpha_c,	alpha_f, beta_f, alpha_derapage, vitesse, position, direction, vitesse_sortie_derapage);	
	}

}
