package li260.radars;
import li260.Voiture.Commande;
import li260.Voiture.Voiture;
import li260.circuit.Circuit;
import li260.geometry.Vecteur;
import li260.geometry.Vtools;
import li260.tools.Tools;

public class RadarClassique implements Radar{
	protected Voiture voiture;
	protected Circuit track;
	protected double[] thetas;
	protected Commande[] allCom;
	protected int bestIndex;
	protected double [] scores;
	protected double epsilon = 0.1;

	public RadarClassique ( Voiture v, Circuit c, double[] thetas, Commande[] allCom){
		super();
		voiture = v;
		track=c;
		this.thetas = thetas;
		this.allCom = allCom;
	}
	public double[] distInPixels() {
		return null;
	}
	
	public double calcScore (double theta){
		Vecteur pos = Vtools.clonage(voiture.getPosition());
		Vecteur dir = Vtools.clonage(voiture.getDirection());
		dir.rotation(theta);
		int cpt = 0;
		while (Tools.isRunnable(track.getTerrain(pos))){
			cpt++;
			pos = Vtools.addition(pos, Vtools.fact(dir,epsilon));
		}
		return cpt;
	}

	public int getBestIndex() {
		return bestIndex;
	}

	public double[] scores() {
		scores = new double[thetas.length];
		bestIndex = 0;
		for(int i=0; i<thetas.length; i++){
			scores[i] = calcScore(thetas[i]);
			if(scores[i]>scores[bestIndex]){
				bestIndex=i;
			}
		}	
		return scores;
	}
	public double[] getThetas() {
		return thetas;
	}
	public Commande[] getAllCom (){
		return allCom;
	}
}
