package li260.strategy;

import li260.Voiture.Commande;
import li260.Voiture.Voiture;
import li260.circuit.Circuit;
import li260.radars.Radar;

public class StrategyRadarParabolique implements Strategy {

	Radar r;
	Voiture v;
	Circuit c;
	
	public StrategyRadarParabolique(Radar r, Voiture v, Circuit c){
		this.r = r;
		this.v = v;
		this.c = c;
	}
	
	public Commande getCommande() {

		double [] scores = r.scores();
		int index = r.getBestIndex();
		Commande c =  r.getAllCom()[index];
		System.out.println("    vitesse Voiture : "+v.getVitesse()+"    dirVoiture : "+v.getDirection()+"   posVoiture : "+v.getPosition());
		double turnAbs = Math.min( Math.abs(c.getTurn()), v.getMaxTurnSansDerapage());
		return new Commande(c.getAcc(), turnAbs * Math.signum(c.getTurn()));
	}

}
