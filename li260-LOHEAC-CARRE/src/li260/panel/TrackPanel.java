package li260.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class TrackPanel extends JFrame {	
	
	public TrackPanel() {	
		
		JButton bouton1 = new JButton("bouton1");
		JButton bouton2 = new JButton("bouton2");
		JLabel message = new JLabel("Bonjour");
		JPanel panneau = new JPanel();
		Box panneauBouton = Box.createVerticalBox();

		panneau.setPreferredSize(new Dimension(100, 0));
		panneau.setBackground(Color.BLUE);
		add(panneau, BorderLayout.CENTER);

		panneauBouton.add(bouton1);
		panneauBouton.add(Box.createVerticalGlue());
		panneauBouton.add(bouton2);
		panneauBouton.add(Box.createGlue());
		panneauBouton.add(message);
		panneauBouton.setBorder(BorderFactory.createLineBorder(Color.RED));
		add(panneauBouton,BorderLayout.EAST );

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(200, 300);
		pack(); 
		setVisible(true);
	}	
	
}
