package li260.strategy;

import li260.radar.Radar;
import li260.tools.Commande;
import li260.voiture.Voiture;

public class StrategyMur implements Strategy {
	private Radar radar;	
	private Voiture car;

	public StrategyMur(Radar radar, Voiture car) {
		super();
		this.radar = radar;
		this.car = car;
	}
	
	@Override
	public Commande getCommande() {
		double acc = c.getAcc();
		if(radar.getToutDroit() < 300 && car.getVitesse()>0.1){
			acc = -1+radar.getToutDroit()/500;
			c.setTurn(c.getTurn()*2);
			System.out.println("td="+radar.getToutDroit());	
			System.out.println("v="+car.getVitesse());	
		}
		return new Commande(acc, turnAbs * Math.signum(c.getTurn()));
		}
	
	}

}
