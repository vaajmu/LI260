package li260.strategy;


import li260.Voiture.Commande;
import li260.Voiture.Voiture;
import li260.geometry.Vecteur;
import li260.geometry.Vtools;
import li260.tools.S;

public class StrategyTestAngles implements Strategy{
	
	int i;
	boolean arrete;
	Voiture v;
	
	public StrategyTestAngles(Voiture v){
		i=0;
		arrete = false;
		this.v = v;
	}
	public Commande getCommande() {
		if(i<70){
			i++;
			S.out(i+"Vitesse voiture : "+v.getVitesse());
			return new Commande(1, 0);
		}
		else if(v.getVitesse()>0.1 && arrete==false){
			S.out("Compteur : "+(i-50));
			i++;
			return new Commande(-1, 0);
		}
		else if(v.getVitesse()<0.1){
			arrete = true;
			double angle = Vtools.angle(v.getDirection(), new Vecteur(0, 1));
			System.out.println("Direction avant :" + angle);
			return new Commande(1, 1);
		}
		else{
			double angle = Vtools.angle(v.getDirection(), new Vecteur(0, 1));
			System.out.println("Direction après :" + angle);
			return new Commande(1, 0);
		}
	}

}
