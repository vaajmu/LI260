package li260.strategy.factory;

import li260.Voiture.Voiture;
import li260.circuit.Circuit;
//import li260.main.TestFile;
import li260.radars.Radar;
import li260.radars.RadarDijkstra;
import li260.selector.SelectorCube;
import li260.selector.SelectorLigneArr;
import li260.selector.SelectorRadarDijkstra;
import li260.selector.SelectorVMin;
import li260.strategy.Strategy;
import li260.strategy.StrategyLigneArr;
import li260.strategy.StrategyRadar;
import li260.strategy.StrategySafe;
import li260.strategy.StrategySelector;
import li260.strategy.StrategyVMin;
import li260.strategy.dijkstra.Dijkstra;
import li260.tools.Faisceaux;

public class GhostFactory {

	Circuit c;
	Voiture v;
	Faisceaux f;
	Dijkstra dijk;
	Radar rd;
	SelectorRadarDijkstra seRadarDijk;

	public GhostFactory(Circuit c, Voiture v, Dijkstra dijk, Faisceaux f){
		this.c=c;
		this.v=v;
		this.f = f;
		this.dijk=dijk;
	}

	public Strategy build(){

		Radar rd = new RadarDijkstra(v, c, dijk, f.getThetas(), f.getAllCom());
		
		StrategySelector stGhost = new StrategySelector();
		
		// Creation des strategies
		StrategyLigneArr stLigArr = new StrategyLigneArr(c, v);
		StrategyRadar stRadarDijk = new StrategyRadar(rd,v);
		StrategyVMin stV = new StrategyVMin(stRadarDijk, v);
		StrategySafe stSafe = new StrategySafe(stRadarDijk);
		
		// Creation des selectors
		SelectorLigneArr seLigArr = new SelectorLigneArr(c, v);
		SelectorVMin seVG = new SelectorVMin(v);
		SelectorRadarDijkstra seRadarDijk = new SelectorRadarDijkstra();
		SelectorCube seC = new SelectorCube(dijk, v);
		
		// Ajout des stratégies et des selectors
		
		// Strat�gie Vitesse minimum
		stGhost.add(stV, seVG);
		
		// Strat�gie safe
//		stGhost.add(stSafe, seC);
		
		// Strat�gie Ligne arriv�e
		stGhost.add(stLigArr, seLigArr);
		
		// Strat�gie radar Dijkstra
		stGhost.add(stRadarDijk, seRadarDijk);

		return stGhost;
	}


}
