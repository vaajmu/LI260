package li260.strategy;

import li260.Voiture.Commande;
import li260.Voiture.Voiture;
import li260.circuit.Circuit;
import li260.radars.Radar;

public class StrategyRadarDijkstra implements Strategy{

	private Voiture voiture;
	private Circuit circuit;
	private Radar radardijk;
	private double [] thetas;
	private Commande [] coms;
	
	public StrategyRadarDijkstra(Radar r,Voiture v, Circuit circuit){
		this.radardijk = r;
		this.circuit = circuit;
		coms = radardijk.getAllCom();
		thetas = radardijk.getThetas();
		voiture =v;
	}

	
	public Commande getCommande() {
		double [] scores = radardijk.scores();
		int index = radardijk.getBestIndex();
		Commande c =  coms[index];
		System.out.println("    vitesse Voiture : "+voiture.getVitesse()+"    dirVoiture : "+voiture.getDirection()+"   posVoiture : "+voiture.getPosition());
		double turnAbs = Math.min( Math.abs(c.getTurn()), voiture.getMaxTurnSansDerapage());
		return new Commande(c.getAcc(), turnAbs * Math.signum(c.getTurn()));
	}

}
