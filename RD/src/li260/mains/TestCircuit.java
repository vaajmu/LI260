package li260.mains;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import swing.CircuitObserver;
import swing.IHMSwing;
import swing.TrackPanel;
import swing.TrajectoireObserver;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;

import li260.algo.Dijkstra;
import li260.circuit.Circuit;
import li260.circuit.CircuitFactory;
import li260.exception.VoitureException;
import li260.ihm.observer.Controleur;
import li260.ihm.observer.DijkObserver;
import li260.ihm.observer.RadarObserver;
import li260.ihm.observer.VoitureObserver;
import li260.radar.Radar;
import li260.radar.RadarClassique;
import li260.radar.RadarDijkstra;
import li260.simulation.Simulation;
import li260.strategy.Strategy;
import li260.strategy.StrategyRadar;
import li260.strategy.StrategyToutDroit;
import li260.tools.Commande;
import li260.tools.Tools;
import li260.voiture.FerrariFactory;
import li260.voiture.Voiture;
import li260.voiture.VoitureFactory;
import li260.voiture.VoitureImpl;

/*+getTerrain(int i, int j): Terrain
+getTerrain(Vecteur p): Terrain
+getPointDepart(): Vecteur
+getDirectionDepart(): Vecteur
+getDirectionArrivee(): Vecteur
+ getWidth(): int
+ getHeight(): int
 */
public class TestCircuit {
	public static void main(String[] args) throws VoitureException, IOException{
		double[] thetas = {0., -Math.PI/2., -Math.PI/3., -Math.PI/4., -Math.PI/6., -Math.PI/8., -Math.PI/16.,
				Math.PI/2., Math.PI/3., Math.PI/4., Math.PI/6., Math.PI/8., Math.PI/16.};
		
		String nom="aufeu";
		
		CircuitFactory cf = new CircuitFactory("tracks/"+nom+".trk");
		Circuit c = cf.build();
		
		FerrariFactory f = new FerrariFactory(c);
		Voiture v =f.build();
		
		Dijkstra dijk = new Dijkstra(c);
		
		Radar r = new RadarDijkstra(thetas, v, c, dijk);
		Radar rc = new RadarClassique(thetas, v, c);
		
		Strategy s = new StrategyRadar(v,r);
		Simulation sim = new Simulation(c, v, s);
		
		IHMSwing ihm = new IHMSwing();
		JFrame fen = new JFrame ();	

		
		ihm.add(new CircuitObserver(c));
		ihm.add(new TrajectoireObserver(v));	
		ihm.add(new VoitureObserver(v)); // Ajout des observeurs	
		ihm.add(new RadarObserver(s.getRadar(), v));
		//ihm.add(new DijkObserver(dijk));

		//simc.add(ihm);
		sim.add(ihm); // Ajout des récepteurs dans l’émetteur	
		//dijk.add(ihm);

		dijk.compute();
		
		//simc.play();
		//*
		ihm.setPreferredSize(new Dimension(768, 1024));	
		fen.setContentPane(ihm);	
		fen.setVisible(true);	
		fen.pack();	
		
		
		sim.play();
		
		//*/
		//ImageIO.write(ihm.getImage(), "png", new File("result.png"));	
		/*
		JFrame fen2 = new JFrame();	
		fen2.add(new JLabel(new ImageIcon(ihm.getImage())));	
		fen2.setVisible(true);	
		fen2.pack();
		//*/
		/*
		TrackPanel panel = new TrackPanel();	
		JFrame fenetre = new JFrame("Simulation LI260 !");	
		fenetre.add(panel);
		fenetre.add(new JLabel(new ImageIcon(ihm.getImage())));
		fenetre.setVisible(true);	
		fenetre.pack();
		//*/
		sim.saveListeCommande(sim.getRecord(), "sauv/"+nom+".com");

	}


}
