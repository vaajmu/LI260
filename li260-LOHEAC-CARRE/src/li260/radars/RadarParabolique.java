package li260.radars;

import li260.Voiture.Commande;
import li260.Voiture.Voiture;
import li260.circuit.Circuit;
import li260.geometry.Vecteur;
import li260.geometry.Vtools;
import li260.strategy.dijkstra.Dijkstra;
import li260.tools.Tools;

public class RadarParabolique implements Radar {
	
	double valMax[];
	Commande allCom[];
	Voiture v;
	Circuit c;
	Dijkstra dijk;
	double yMax;
	double[] scores;
	int bestIndex;
	
	public RadarParabolique(Voiture v, Circuit c, Dijkstra dijk, double[] vM, Commande[] aC){
		this.v = v;
		this.c = c;
		this.dijk = dijk;
		this.valMax = vM;
		this.allCom = aC;
		this.yMax = 50;
	}

	public double[] scores() {
		scores = new double[valMax.length];
		bestIndex = 0;
		for(int i=0; i<valMax.length; i++){
			scores[i] = calcScore(valMax[i]);
			if(scores[i]<scores[bestIndex])
				bestIndex=i;
		}	
		return scores;
	}

	public double calcScore(double xMax){
		Vecteur pos;
		Vecteur dir;
		double best = Double.POSITIVE_INFINITY, i = 0;
		double x, y;
		double b = (2*yMax)/xMax;
		double a = -b/(2*xMax);
		while (i<100){
			// Calcul de x et y
			x = i*(xMax/50);
			y = a*x*x + b*x;
			// Changement de plan
			dir = transformation(x, y);
			// Nouvelle position
			pos = Vtools.addition(v.getPosition(), dir);
			// Test de runnabilitée
			if(!Tools.isRunnable(c.getTerrain(pos)))
				break;
			else{
				if(dijk.getDist(pos) < best)
					best = dijk.getDist(pos);
			}
			i++;
		}
		return best;
	}
	private Vecteur transformation(double x, double y){
		double angle = Vtools.angle(new Vecteur(0, 1), v.getDirection());
		Vecteur dir = new Vecteur(x, y);
		dir.rotation(angle);
		return dir;
	}
	public double[] distInPixels() {
		return null;
	}
	public int getBestIndex() {
		return bestIndex;
	}
	public double[] getThetas() {
		return valMax;
	}
	public Commande[] getAllCom() {
		return allCom;
	}
}
