package li260.panel;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class OptionPanel extends JPanel{
	
	JPanel panel;
	JLabel texte;
	SpinnerNumberModel model;
	JSpinner field;
	
	public OptionPanel(String nom, double defaut, double min, double max, double pas){
		super();
		
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(105, 50));
		panel.setLayout(new FlowLayout());
		setVisible(true);
		
		texte = new JLabel(nom);
		model = new SpinnerNumberModel(defaut, min, max, pas); 
		field = new JSpinner(model);

		panel.add(texte);
		panel.add(field);
	}
	public JPanel build(){
		
		return panel;
	}
	public SpinnerNumberModel getSpinner(){
		return model;
	}
}
