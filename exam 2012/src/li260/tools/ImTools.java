package li260.tools;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import li260.Voiture.Commande;
import li260.Voiture.Voiture;
import li260.circuit.Circuit;

public class ImTools{

	public static BufferedImage imageFromCircuit(Circuit c) {

		try{

			BufferedImage im = new BufferedImage(c.getHeight(),c.getWidth(),BufferedImage.TYPE_INT_ARGB);
			for(int j=0;j<c.getHeight();j++){
				for(int i=0;i< c.getWidth() ;i++){
					im.setRGB(j,i, Tools.colorFromTerrain(c.getTerrain(j, i)));
				}
			}
			return im;
		} 

		catch (Exception e) {
			System.err.println ("erreur inconnue: " + "fichier" + "... Loading aborted");e.printStackTrace();
		}
		return null;
	}

	public static BufferedImage imageAvecVoiture (BufferedImage im, Voiture v){
		im.setRGB((int)v.getPosition().getX(), (int)v.getPosition().getY(), Color.orange.getRGB());
		return im;
	}

	public static void saveListeCommande(ArrayList<Commande> liste, String filename){
		try {
			int cpt = 0;
			DataOutputStream os = new DataOutputStream(new FileOutputStream(filename));
			for(Commande c:liste){
				os.writeDouble(c.getAcc());
				os.writeDouble(c.getTurn());
				cpt++;
			}
			System.out.println("nb de commandes = " + cpt);
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static ArrayList<Commande> loadListeCommande(String filename) throws IOException{
		ArrayList<Commande> liste = null;

		try {
			DataInputStream os = new DataInputStream(new FileInputStream(filename));

			liste = new ArrayList<Commande>();
			double a,t;
			while(true){ // on attend la fin de fichier
				a = os.readDouble();
				t = os.readDouble();
				liste.add(new Commande(a,t));
			}

		} catch (EOFException e){
			return liste;
		} 

	}

}

