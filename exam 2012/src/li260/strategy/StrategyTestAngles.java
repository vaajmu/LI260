package li260.strategy;

import java.util.ArrayList;

import li260.Voiture.Commande;
import li260.Voiture.Voiture;
import li260.geometry.Vecteur;
import li260.geometry.Vtools;

public class StrategyTestAngles implements Strategy{
	
	int i;
	boolean arrete;
	Voiture v;
	
	public StrategyTestAngles(Voiture v){
		i=0;
		arrete = false;
		this.v = v;
	}
	@Override
	public Commande getCommande() {
		if(i<50){
			i++;
			return new Commande(1, 0);
		}
		else if(v.getVitesse()>0.1 && arrete==false)
			return new Commande(-1, 0);
		else if(v.getVitesse()<0.1){
			arrete = true;
			double angle = Vtools.angle(v.getDirection(), new Vecteur(0, 1));
			System.out.println("Direction avant :" + angle);
			return new Commande(1, 1);
		}
		else{
			double angle = Vtools.angle(v.getDirection(), new Vecteur(0, 1));
			System.out.println("Direction après :" + angle);
			return new Commande(1, 0);
		}
	}

}
