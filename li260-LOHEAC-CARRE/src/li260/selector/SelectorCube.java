package li260.selector;

import li260.Voiture.Voiture;
import li260.geometry.Vecteur;
import li260.geometry.Vtools;
import li260.strategy.dijkstra.Dijkstra;
import li260.tools.S;

public class SelectorCube implements Selector {

	Dijkstra dijk;
	Voiture v;
	private int avance;
	private double angle;
	
	public SelectorCube(Dijkstra dijk, Voiture v){
		this.dijk = dijk;
		this.v = v;
		avance = 20;
		angle = 4;
	}
	
	public boolean isSelected() {
		Vecteur pos = Vtools.clonage(v.getPosition());
		for(int i=0; i<avance; i++){
			pos = nextPos(pos);
		}
		double angle = Vtools.angle(v.getDirection(), new Vecteur(v.getPosition(), pos));
		if(Math.abs(angle) > Math.PI/angle){
			S.out("Cube");
			return true;
		}
		return false;
	}
	private Vecteur nextPos(Vecteur pos){
		Vecteur bestV = new Vecteur(pos.getX(), pos.getY());
		double bestS = dijk.getDist(pos);
		
		for(int i=(-1); i<=1; i++){
			for(int j=(-1); j<=1; j++){
				if(dijk.getDist((int) pos.getX()+i, (int) pos.getY()+j) < bestS){
					bestS = dijk.getDist((int) pos.getX()+i, (int) pos.getY()+j);
					bestV.setX(pos.getX()+i);
					bestV.setY(pos.getY()+j);
				}
			}
		}
		return bestV;
	}

}
