package li260.geometry;

public class Vecteur {
	private double x;
	private double y;
	
	public Vecteur(double x, double y){
		this.x = x;
		this.y = y;
	}
	public Vecteur(Vecteur a, Vecteur b){
		x = (b.x-a.x);
		y = (b.y-a.y);
	}
	public void rotation(double theta){
		double temp = x;
		x = temp*Math.cos(theta)-y*Math.sin(theta);
		y = temp*Math.sin(theta)+y*Math.cos(theta);
	}
	
	public void setX(double x) {
		this.x = x;
	}
	public void setY(double y) {
		this.y = y;
	}
	public double getX(){
		return x;
	}
	public double getY(){
		return y;
	}
	public String toString(){
		return super.toString()+ "("+x+";"+y+")";
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vecteur other = (Vecteur) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		if(this.x != other.x)
			return false;
		if(this.y != other.y)
			return false;
		
		return true;
	}
}
