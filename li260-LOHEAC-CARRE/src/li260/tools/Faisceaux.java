package li260.tools;

import li260.Voiture.Commande;

public class Faisceaux {

	double[] thetas;
	Commande[] commandes;
	
	public Faisceaux(int n, double div){
		thetas = definirThetas(n);
		commandes = definirCommandes(thetas, div);
	}

	public double[] getThetas(){
		return thetas;
	}
	public Commande[] getAllCom(){
		return commandes;
	}
	
	public double[] definirThetas(int n){
		int i;
		double[] thetas = new double[2*n+1];

		thetas[0] = 0;
		for(i=1;i<=n;i++){
			thetas[i] = (i*Math.PI)/(2*n);
			thetas[n+i] = -(i*Math.PI)/(2*n);
		}
		return thetas;
	}

	public Commande[] definirCommandes(double[] thetas, double div){
		int i;
		Commande[] com = new Commande[thetas.length];
		for(i=0;i<thetas.length;i++){
			if(Math.abs(thetas[i]) == 0){
				com[i] = new Commande(1, 0);
			}
			else if(Math.abs(thetas[i]) < 0.2){
				com[i] = new Commande(1, thetas[i]/0.2);
			}
			else{
				com[i] = new Commande(1, 1*Math.signum(thetas[i]));
			}
			if(Math.abs(thetas[i])>=Math.PI/div)
				com[i].setAcc(-1);
		}
		return com;
	}
}
