package li260.strategy;

import java.util.ArrayList;
import java.util.concurrent.PriorityBlockingQueue;

import li260.Voiture.Voiture;
import li260.circuit.Circuit;
import li260.geometry.Vecteur;
import li260.geometry.Vtools;
import li260.ihm.observer.UpdateEventListener;
import li260.tools.Tools;

public class Dijkstra {

	private Circuit circuit;
	private double [][] dist;
	private Vecteur current;
	private PriorityBlockingQueue<Vecteur> q;
	private ArrayList<UpdateEventListener> list;


	public Dijkstra(Circuit c){

		super();
		circuit=c;
		dist = new double [c.getHeight()][c.getWidth()];
		q = new PriorityBlockingQueue<Vecteur> (1000,new ComparatorDijk(dist));
		list = new ArrayList<UpdateEventListener>();

//		Mettre les valeurs à +oo
		for(int i=0;i<c.getHeight();i++){
			for(int j=0;j<c.getWidth();j++){
				dist[i][j] =  Double.POSITIVE_INFINITY;
			}
		}
//		Mettre les valeurs de la ligne d'arrivee a 0
		for (int i=0; i<c.getArrivees().size(); i++){
			Vecteur a = c.getArrivees().get(i);
			dist[(int) a.getX()][(int) a.getY()]=0;
			q.add(a);
		}
	}

	public void compute(){
		while (!q.isEmpty()){
			current = q.element();
			updateDist();
			q.remove(current);
			update();
		}

	}

	public void updateDist(){
		for(int i=-1; i<=1; i++){
			for(int j=-1; j<=1; j++){
				Vecteur suiv = new Vecteur(current.getX()+i, current.getY()+j);
				//				On ne traite pas les cases futiles
				if((int)suiv.getX()<0 || (int)suiv.getX()>=circuit.getHeight() || (int)suiv.getY()<0 || (int)suiv.getY()>=circuit.getWidth())
				if(i==0 && j==0) continue;
				//if(/*circuit.getArrivees().contains(current) &&*/ (Vtools.prodScal( new Vecteur(current ,new Vecteur(i,j)), circuit.getDirectionArrivee() )>0)) continue;
				if(circuit.getArrivees().contains(current) && (Vtools.prodScal( new Vecteur(i,j), circuit.getDirectionArrivee() )>0)) continue;
				if(!Tools.isRunnable(circuit.getTerrain((int)suiv.getX(),(int)suiv.getY()))) continue;

				//				Si la valeur absolue des indices sont égales : dist += 14
				if( Math.abs(i)==Math.abs(j) ){
					if(dist[ (int)suiv.getX() ][ (int)suiv.getY() ] == Double.POSITIVE_INFINITY){
						dist[(int) suiv.getX() ][(int) suiv.getY() ] = dist[(int) (current.getX())][(int) (current.getY())] + 14;
						q.add(suiv);
					}
					if(dist[(int) current.getX()][(int) current.getY()]+14 < dist[(int) suiv.getX() ][(int) suiv.getY() ] ){
						q.remove(suiv);
						dist[(int) suiv.getX()][(int) suiv.getY() ] = dist[(int) (current.getX())][(int) (current.getY())] + 14;
						if(!suiv.equals(q.peek()))
							q.add(suiv);
					}
				}
				//				Sinon dist += 10
				else{
					if(dist[ (int)suiv.getX() ][ (int)suiv.getY() ] == Double.POSITIVE_INFINITY){
						dist[(int) suiv.getX() ][(int) suiv.getY() ] = dist[(int) (current.getX())][(int) (current.getY())] + 10;
						q.add(suiv);
					}
					if(dist[(int) current.getX()][(int) current.getY()]+14 < dist[(int) suiv.getX() ][(int) suiv.getY() ] ){
						q.remove(suiv);
						dist[(int) suiv.getX()][(int) suiv.getY() ] = dist[(int) (current.getX())][(int) (current.getY())] + 10;
						if(!suiv.equals(q.peek()))
							q.add(suiv);
					}
				}
			}
		}
	}
	public Vecteur getCurrent(){
		return current;
	}
	public double getDist(int i, int j){
		return dist[i][j];
	}
	public void update() {
		for(UpdateEventListener listener: list)	
			listener.manageUpdate();
	}
	public void add(UpdateEventListener l) {
		list.add(l);
	}
}




