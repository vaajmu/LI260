package li260.Voiture;

import li260.circuit.Circuit;
import li260.geometry.Vecteur;

public interface Voiture {
	// pour le pilotage
	public void drive(Commande c) throws VoitureException;
	public double getMaxTurnSansDerapage();

	// pour l'observation
	public double getVitesse();
	public Vecteur getPosition();
	public Vecteur getDirection();
	public boolean getDerapage();
	public double getBraquage();
	public boolean arriveeBonSens(Circuit track);
	public double getVpourB();
	public Commande getComPrec();
	
	public Voiture clone();
//	public void setVitesse(double vitesse);
//	public void setDerapage(boolean derapage);
//	public void setPosition(Vecteur position);
//	public void setDirection(Vecteur direction);

}


