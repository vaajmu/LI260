package li260.selector;

import li260.Voiture.Voiture;
import li260.tools.S;

public class SelectorVMin implements Selector {

	Voiture v;
	
	public SelectorVMin(Voiture v){
		this.v = v;
	}
	
	public boolean isSelected() {
		if(v.getVitesse() < 0.2){
			S.out("VMIN");
			return true;
		}
		return false;
	}

}
