package li260.selector;

import li260.circuit.Circuit;
import li260.voiture.Voiture;

public class SelectorEndLine implements Selector {
	private Circuit c;
	private Voiture v;
	
	public SelectorEndLine(Circuit c, Voiture v) {
		super();
		this.c = c;
		this.v = v;
	}

	public boolean isSelected() {
	
		return false;
	}

}
