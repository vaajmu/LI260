package li260.strategy;

import java.util.ArrayList;

import li260.Voiture.Commande;
import li260.Voiture.Voiture;
import li260.circuit.Circuit;
import li260.circuit.Terrain;
import li260.geometry.Vecteur;
import li260.geometry.Vtools;
import li260.tools.Tools;

public class StrategyLigneArr implements Strategy{

	private Circuit c;
	private Voiture v;
	private ArrayList<Vecteur> listArr;
	private ArrayList<Integer> scores;
	private double epsilon=0.1;
	private int bestI;

	public StrategyLigneArr (Circuit c,Voiture v){
		this.c = c;
		listArr=c.getArrivees();
		this.v=v;
		

	}

	public Commande getCommande(){
		scores();
		
		Vecteur bestArr = Vtools.clonage(listArr.get(bestI));
		Vecteur dirArr = new Vecteur(v.getPosition(), bestArr);
		dirArr=Vtools.normaliser(dirArr);
		double angle = Vtools.angle(v.getDirection(), dirArr);
		Commande com = new Commande(1, angle);
		double turnAbs = Math.min( Math.abs(com.getTurn()), v.getMaxTurnSansDerapage());
		if (Double.isNaN(com.getTurn()) || Double.isNaN(turnAbs*Math.signum(com.getTurn())) ){
			com.setTurn(0.0);
		}
		return new Commande(com.getAcc(), turnAbs*Math.signum(com.getTurn()) );
	}

	public int calcScore(Vecteur dir){
		Vecteur pos = Vtools.clonage(v.getPosition());
		int cpt = 0;
		while (Tools.isRunnable(c.getTerrain(pos))){
			if(c.getTerrain(pos)==Terrain.EndLine){
				return cpt;
			}
			cpt++;
			pos = Vtools.addition(pos, Vtools.fact(dir,epsilon));
		}
		return Integer.MAX_VALUE;		
	}

	public void  scores() {
		int i=0;
		bestI=0;
		scores = new ArrayList<Integer>();
		Vecteur vdir = Vtools.clonage(v.getDirection());
		vdir = Vtools.normaliser(vdir);
		for(Vecteur arr : listArr){
			Vecteur dirArr = new Vecteur(v.getPosition(), arr);
			dirArr = Vtools.normaliser(dirArr);
			scores.add(calcScore(dirArr));
			if(scores.get(i)<scores.get(bestI)){
				bestI=i;
			}
			i++;
		}
	}
}

