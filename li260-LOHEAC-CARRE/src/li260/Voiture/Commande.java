package li260.Voiture;

public class Commande {
	private double acc;
	private double turn;
	
	public Commande (double acceleration,double rotation){
		super();
		acc=acceleration;
		turn=rotation;
	}
	
	public Commande (Commande c){
		this(c.getAcc(), c.getTurn());
	}
	
	public double getAcc() {
		return acc;
	}
	public double getTurn() {
		return turn;
	}
	public void setAcc(double acceleration) {
		this.acc = acceleration;
	}

	public void setTurn(double rotation) {
		this.turn = rotation;
	}
}
