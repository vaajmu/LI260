package li260.geometry;

import li260.circuit.Circuit;
import li260.circuit.Terrain;
import li260.tools.Tools;

public class Vtools {

	public static double norme(Vecteur v){
		return Math.sqrt((v.getX()*v.getX())+(v.getY()*v.getY()));
	}
	public static Vecteur normaliser(Vecteur v){
		return new Vecteur(v.getX()/norme(v),v.getY()/norme(v));
	}
	public static double prodScal(Vecteur a, Vecteur b){
		return a.getX()*b.getX()+a.getY()*b.getY();
	}
	public static Vecteur clonage(Vecteur a){
		return new Vecteur(a.getX(), a.getY());
	}
	public static Vecteur addition(Vecteur a, Vecteur b){
		double x,y;
		x=a.getX()+b.getX();
		y=a.getY()+b.getY();
		Vecteur v = new Vecteur (x,y);
		return v;
	}
	public static Vecteur fact(Vecteur dir, double vitesse){
		return new Vecteur(vitesse*dir.getX(),vitesse*dir.getY());
	}
	public static Vecteur add(Vecteur pos, Vecteur fact){
		return addition (pos,fact);
	}
	
	
	public static boolean seCroisent(Vecteur a, Vecteur b, Vecteur c, Vecteur d){
		Vecteur ca = new Vecteur(c, a);
		Vecteur cd = new Vecteur(c, d);
		Vecteur cb = new Vecteur(c, b);
		Vecteur ac = new Vecteur(a, c);
		Vecteur ab = new Vecteur(a, b);
		Vecteur ad = new Vecteur(a, d);
		
		if( getZ(ca, cd)*getZ(cb, cd)<0 && getZ(ac, ab)*getZ(ad, ab)<0 )
			return true;
		else
			return false;
	}
	
	public static Vecteur soustraction(Vecteur a, Vecteur b){
		double x,y;
		x=a.getX()-b.getX();
		y=a.getY()-b.getY();
		Vecteur v = new Vecteur (x,y);
		return v;
	}
	public static boolean isOthogonaux(Vecteur a, Vecteur b){
		if (prodScal(a,b)==0){
			return true;
		}
		else return false;
	}
	public static double getZ(Vecteur a, Vecteur b){
		return (a.getX()*b.getY())-(a.getY()*b.getX());
	}
	public static double angle(Vecteur a, Vecteur b){
		a=normaliser(a);
		b=normaliser(b);
		double theta = Math.acos(prodScal(a, b)/*/(norme(a)*norme(b))*/);
		if( Double.isNaN(theta)){
			theta=0;
		}
		if(getZ(a, b)<0)
			return -theta;
		else
			return theta;
	}
	public static Vecteur vecteurRandom (){
		double a, b;
		do{
			a = (Math.random()*3)-1;
		}while(a>1);
		do{
			b = (Math.random()*3)-1;
		}while(b>1);
		return new Vecteur(a, b);
	}
}
