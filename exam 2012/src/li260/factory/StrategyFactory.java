package li260.factory;

//import li260.main.TestFile;
//import examen2012.MainExo1AL;
//import examen2012.MainExo4AL;
import examen2012.MainExo3AL;
import li260.Voiture.Voiture;
import li260.circuit.Circuit;
import li260.selector.SelectorLigneArr;
import li260.selector.SelectorRadarDijkstra;
import li260.selector.SelectorVMin;
import li260.strategy.Dijkstra;
import li260.strategy.Radar;
import li260.strategy.RadarDijkstra;
import li260.strategy.Strategy;
import li260.strategy.StrategyLigneArr;
import li260.strategy.StrategyRadar;
import li260.strategy.StrategySelector;
import li260.strategy.StrategyVMin;

public class StrategyFactory {

	Circuit c;
	Voiture v;
	Dijkstra dijk;
	Radar rd;
	SelectorRadarDijkstra seRadarDijk;

	public StrategyFactory(Circuit c, Voiture v, Dijkstra dijk){
		this.c=c;
		this.v=v;
		this.dijk=dijk;
	}

	public Strategy build(){

		Radar rd = new RadarDijkstra(v, c, dijk, MainExo3AL.thetas, MainExo3AL.allCom);
		
		StrategySelector stGhost = new StrategySelector();
		// Creation des strategies
		StrategyLigneArr stLigArr = new StrategyLigneArr(c, v);
		StrategyRadar stRadarDijk = new StrategyRadar(rd,v);
		StrategyVMin stV = new StrategyVMin(stRadarDijk, v);
		// Creation des selectors
		SelectorLigneArr seLigArr = new SelectorLigneArr(c, v);
		SelectorVMin seVG = new SelectorVMin(v);
		SelectorRadarDijkstra seRadarDijk = new SelectorRadarDijkstra();
		// Ajout des stratégies et des selectors
		stGhost.add(stV, seVG);
		stGhost.add(stLigArr, seLigArr);
		stGhost.add(stRadarDijk, seRadarDijk);

		return stGhost;
	}


}
