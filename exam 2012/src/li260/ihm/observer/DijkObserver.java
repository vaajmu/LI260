package li260.ihm.observer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import li260.strategy.Dijkstra;

public class DijkObserver implements Observer, SwingObserver{

	private Dijkstra dijk;
	
	public DijkObserver (Dijkstra dijk){
		super();
		this.dijk = dijk;
	}

	public void print(BufferedImage im) {
		
		im.setRGB(getX(), getY(), getColor().getRGB());

	}
	public int getX(){
		return (int)dijk.getCurrent().getX();
	}
	public int getY(){
		return (int)dijk.getCurrent().getY();
	}
	public Color getColor(){
		return new Color((int) (dijk.getDist(getX(), getY()) % 255.), 0, 0);
	}

	public void paint(Graphics g) {
	//	g.drawImage(im, 0, 0, null);
		
	}
}
