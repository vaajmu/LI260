package li260.panel;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class TrackPanel extends JPanel {	
	
	public TrackPanel() {	
		super();	
		this.setPreferredSize(new Dimension(1024, 768));	
		setVisible(true);	
	}	
	
}
