package li260.tools;

import java.awt.Dimension;
import java.awt.Point;

public class Coord {
	private static int refX = 1400;
	private static int refY = 900;
	private static Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
	private static int dimX = (int)screenSize.getWidth();
	private static int dimY = (int)screenSize.getHeight();
	
	public static Dimension getDimension(int x, int y){
		return new Dimension(newX(x), newY(x));
	}
	public static Point getPoint(int x, int y){
		return new Point(newX(x), newY(x));
	}
	public static int newX(int x){
		return x*dimX/refX;
	}
	public static int newY(int y){
		return y*dimY/refY;
	}
	public static int getX(){
		return dimX;
	}
	public static int getY(){
		return dimY;
	}
}
