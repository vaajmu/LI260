package li260.strategy;

import li260.Voiture.Commande;

public class StrategySafe implements Strategy{
	
	Strategy str;
	
	public StrategySafe(Strategy str){
		this.str = str;
	}
	
	public Commande getCommande() {
		Commande com = str.getCommande();
		com.setAcc(-1);
		return com;  
	}

}
