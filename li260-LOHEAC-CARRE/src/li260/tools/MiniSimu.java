package li260.tools;

import li260.Voiture.Commande;
import li260.Voiture.Voiture;
import li260.Voiture.VoitureException;
import li260.circuit.Circuit;
import li260.circuit.Terrain;
import li260.strategy.Strategy;

public class MiniSimu{

	private Voiture v;
	private Circuit c;
	private Strategy str; 
	private int nbCoups;

	public MiniSimu(Strategy str, Voiture v, Circuit c, int nbCoups){
		super();
		this.v = v;
		this.c = c;
		this.str = str;
		this.nbCoups = nbCoups;
	}
	public boolean play(){
		for(int i=nbCoups;i>0;i--){
			playOneShot();
			if (!Tools.isRunnable(c.getTerrain(v.getPosition()))){
				return true;
			}
			else if(c.getTerrain(v.getPosition())==Terrain.EndLine){
				if (v.arriveeBonSens(c)){
					return false;
				}
				else{
					return true;
				}
			}
		}
		return false;
	}

	private void playOneShot(){

		Commande c = new Commande(str.getCommande());
		try{
			v.drive(c);
		} catch (VoitureException e){
			System.out.println("Erreur voiture exception");
		}
	}
}
