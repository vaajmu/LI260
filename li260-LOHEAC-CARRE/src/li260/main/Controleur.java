package li260.main;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import li260.ihm.observer.CircuitObserver;
import li260.ihm.observer.IHMSwing;
import li260.ihm.observer.TrajetObserver;
import li260.ihm.observer.VoitureObserver;
import li260.panel.Menu;
import li260.panel.PanelInterface;
import li260.tools.Coord;


public class Controleur implements ActionListener{	

	private Jeu jeu;
	private PanelInterface ihm;
	private IHMSwing ihmSwing;
	private JFrame fen;
	private Menu menu;
	private int larg;

	public Controleur(){
		larg = 90;
		creerMenu();
	}

	public void lancement(){
		// Jeu
		creerJeu();
		// Interface
		creerInterface();
	}
	public void jeu(){
		// Play
		jeu.play();
	}
	public void creerJeu(){
		// Jeu
		jeu = new Jeu(menu.getListe().getSelectedItem().toString());
	}
	public void creerInterface(){
		// IHM
		ihmSwing = new IHMSwing();
		// Observeurs
		ihmSwing.add(new VoitureObserver(jeu.getV()));
		ihmSwing.add(new CircuitObserver(jeu.getC()));
		ihmSwing.add(new TrajetObserver(jeu.getV()));
		// Recepteurs
		jeu.add(ihmSwing); 
		// Fenetre
		creerIhm();
	}
	public IHMSwing getIhm(){
		return ihmSwing;
	}
	public void creerMenu(){
		fen = new JFrame("Menu");
		fen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fen.setPreferredSize(Coord.getDimension(300, 300));
		fen.pack();
		fen.setVisible(true);
		fen.setLocation((Coord.getY()-fen.getWidth()) , 0);
		menu = new Menu(fen);
		fen.setContentPane(menu);
		menu.addActionListener(this);
		menu.revalidate();
	}
	public void creerIhm(){
		fen.removeNotify();
		fen = new JFrame("Simulation LI260!");
		fen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fen.setPreferredSize(new Dimension(jeu.getC().getHeight() +larg+10, jeu.getC().getWidth()));
		fen.setVisible(true);
		fen.pack();
		fen.setLocation((Coord.getY()-fen.getWidth()) , 0);
		// PanelInterface
		ihm = new PanelInterface(fen, ihmSwing, larg);
		ihm.addActionListener(this);
		fen.setContentPane(ihm);
	}
	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();

		if(source == menu.getValider()){
			lancement();
		}
		else if(source == menu.getListe() || source == menu.getRafraichir()){
			menu.rafraichirIm();
		}
		else if(source == ihm.getValider()){
			jeu();
		}
	}

}

