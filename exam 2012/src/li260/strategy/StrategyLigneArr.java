package li260.strategy;

import java.util.ArrayList;

import li260.Voiture.Commande;
import li260.Voiture.Voiture;
import li260.circuit.Circuit;
import li260.circuit.Terrain;
import li260.geometry.Vecteur;
import li260.geometry.Vtools;
import li260.tools.Tools;

public class StrategyLigneArr implements Strategy{

	private Circuit c;
	private Voiture v;
	private ArrayList<Vecteur> listArr;
	private ArrayList<Integer> scores;
	private double epsilon=0.1;
	private int bestI;

	public StrategyLigneArr (Circuit c,Voiture v){
		this.c = c;
		listArr=c.getArrivees();
		this.v=v;
		

	}

	public Commande getCommande(){
		scores();
		
//		System.out.println("    bestI = "+bestI+" (score: "+scores.get(bestI)+") ");
		//System.out.println("score(bestI) = ");
//		System.out.println("    size : "+listArr.size());
//		System.out.println("    vitesse Voiture : "+v.getVitesse()+"    \ndirVoiture : "+v.getDirection()+"   \nposVoiture : "+v.getPosition());
		
		Vecteur bestArr = Vtools.clonage(listArr.get(bestI));
		Vecteur dirArr = new Vecteur(v.getPosition(), bestArr);
		dirArr=Vtools.normaliser(dirArr);
		double angle = Vtools.angle(v.getDirection(), dirArr);
		Commande com = new Commande(1, angle);
//		System.out.print("    angle"+angle);
		double turnAbs = Math.min( Math.abs(com.getTurn()), v.getMaxTurnSansDerapage());
//		System.out.print("    acc="+com.getAcc());
//		System.out.println("    turn="+turnAbs*Math.signum(com.getTurn()));
		//Tools.sop("Terrain \t: " + Tools.stringFromTerrain(c.getTerrain(v.getPosition())));
		//Tools.sop("Coordonnées \t: "+(int)v.getPosition().getX()+ " , " +(int)v.getPosition().getY());
		if (Double.isNaN(com.getTurn()) || Double.isNaN(turnAbs*Math.signum(com.getTurn())) ){
			com.setTurn(0.0);
		}
		//Tools.sop("Commande \t: " + "( "+ com.getAcc()+" ; "+com.getTurn()+" )");
		//comPrec = com;
		return new Commande(com.getAcc(), turnAbs*Math.signum(com.getTurn()) );
		//return com;
	}

	public int calcScore(Vecteur dir){
		Vecteur pos = Vtools.clonage(v.getPosition());
		int cpt = 0;
		while (Tools.isRunnable(c.getTerrain(pos))){
			if(c.getTerrain(pos)==Terrain.EndLine){
//				System.out.println("  score : "+cpt);
				return cpt;
			}
			cpt++;
			pos = Vtools.addition(pos, Vtools.fact(dir,epsilon));
		}
		//System.out.println("  score : max_value: "+cpt);
		return Integer.MAX_VALUE;		
	}
	
	/* //ISVISIBLE
	double epsilon = 0.1;
	Vecteur pos = Vtools.clonage(position);
	Vecteur dir = new Vecteur(pos, but);
	dir = Vtools.normaliser(dir);
	//		System.out.println("Vérification visible");
	while (Tools.isRunnable(this.getTerrain(pos))){
		//		System.out.println("Test terrain réussi");
		if(this.getTerrain(pos)==Terrain.EndLine){
					//System.out.println("endline is Visible ");
			return true;
		}
		pos = Vtools.addition(pos, Vtools.fact(dir, epsilon));
//		if(pos.getX() >= this.getHeight() || pos.getX() < 0)
//			System.out.println("Erreur borne X : " + pos.getX());
//		if(pos.getY() >= this.getWidth() || pos.getY() < 0)
//			System.out.println("Erreur borne Y : " + pos.getY());
	}
	return false;*/

	public /*ArrayList<Integer>*/ void  scores() {
		int i=0;
		bestI=0;
		scores = new ArrayList<Integer>();
		Vecteur vdir = Vtools.clonage(v.getDirection());
		vdir = Vtools.normaliser(vdir);
		for(Vecteur arr : listArr){
			Vecteur dirArr = new Vecteur(v.getPosition(), arr);
			dirArr = Vtools.normaliser(dirArr);
			//System.out.print("  i="+i+"  ");
			scores.add(calcScore(dirArr));
			if(scores.get(i)<scores.get(bestI)){
				bestI=i;
			}
			i++;
		}
		//return scores;
		
		/*public double[] scores() {
			scores = new double[thetas.length];
			bestIndex = 0;
			for(int i=0; i<thetas.length; i++){
				scores[i] = calcScore(thetas[i]);
				if(scores[i]>scores[bestIndex]){
//				if(scores[i]>=scores[bestIndex]){
					bestIndex=i;
				}
			}	
			return scores;
		}*/
	}
}

