package li260.selector;

import li260.Voiture.Voiture;
import li260.circuit.Circuit;
import li260.geometry.Vecteur;
import li260.geometry.Vtools;
import li260.tools.S;

public class SelectorLigneArr implements Selector{

	private Circuit c;
	private Voiture v;

	public SelectorLigneArr (Circuit c,Voiture v){
		this.c = c;
		this.v = v;
	}

	public boolean isSelected() {
		//int i = (int)(Math.random()*c.getArrivees().size());
		int i;
		//System.err.println("strlignearr?");
		for(i=0; i < c.getArrivees().size() ; i++){
			if( c.isVisible(v.getPosition(), c.getArrivees().get(i)) && 
					(Vtools.prodScal(new Vecteur(v.getPosition(),c.getPointDepart()), c.getDirectionArrivee()) >0) && 
					Vtools.prodScal(v.getDirection(), c.getDirectionArrivee())>0){
				S.out("ligneArr");
				return true;
			}
		}
		return false;
	}
}
