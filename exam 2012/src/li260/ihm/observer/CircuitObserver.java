package li260.ihm.observer;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import li260.circuit.Circuit;
import li260.tools.ImTools;


public class CircuitObserver implements Observer,SwingObserver {

	private BufferedImage im;

	public CircuitObserver(Circuit track) {
		super();
		im = ImTools.imageFromCircuit(track);
	}
	
	public void print(BufferedImage im) {
		
	}

	public void paint(Graphics g) {
		
		g.drawImage(im, 0, 0, null);
	}

}
