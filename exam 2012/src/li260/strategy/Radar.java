package li260.strategy;
import li260.Voiture.Commande;

public interface Radar {
	
	public double [] scores();
	public double []distInPixels();
	public int getBestIndex();
	public double [] getThetas();
	public Commande [] getAllCom();
	
}
