package li260.strategy;

import li260.Voiture.Commande;
import li260.Voiture.Voiture;

public class StrategyVMin implements Strategy {

	StrategyRadar stratRadarDijk;
	Voiture v;

	public StrategyVMin(StrategyRadar stratRadarDijk, Voiture v){
		this.stratRadarDijk = stratRadarDijk;
		this.v = v;
	}

	@Override
	public Commande getCommande() {
		Commande com = stratRadarDijk.getCommande();
		com.setAcc(1);
		return com;  
	}

}
