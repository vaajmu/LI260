package li260.Voiture;

import li260.circuit.Circuit;
import li260.geometry.Vecteur;
import li260.geometry.Vtools;

public class VoitureImpl implements Voiture{
	private double vitesse;
	private boolean derapage;
	private Vecteur position;
	private Vecteur direction;
	private Commande comPrec;
	
	private double vmax;
	private double alpha_c;
	private double braquage;
	private double alpha_f;
	private double beta_f;
	private double alpha_derapage;
	private double masse;
	private double vitesse_sortie_derapage;
	private double[] tabVitesse 	= {0.1,	0.2, 0.3, 0.4, 0.5,	0.6, 0.7, 0.8,	0.9, 1.}; 
	private double[] tabTurn 	= {1., 1., 0.8, 0.7, 0.6, 0.4,0.3,0.2, 0.1, 0.075};
	private double vitesseMinBraquageMax;
	
	
	public VoitureImpl(double vmax, double braquage, double alpha_c,
			double alpha_f, double beta_f, double alpha_derapage,
			double vitesse, Vecteur position, Vecteur direction, double vitesse_sortie_derapage) {
		super();
		this.vmax = vmax;
		this.alpha_c = alpha_c;
		this.braquage = braquage;
		this.alpha_f = alpha_f;
		this.beta_f = beta_f;
		this.alpha_derapage = alpha_derapage;
		this.masse = masse;
		this.vitesse_sortie_derapage = vitesse_sortie_derapage;
		this.tabVitesse = tabVitesse;
		this.tabTurn = tabTurn;
		this.direction = direction;
		this.position=position;
		this.vitesse=vitesse;
		vitesseMinBraquageMax = tabVitesse[2];
	}

	private void driveSansDerapage(Commande c) {
		// approche normale
		// 1) gestion du volant
		direction.rotation(c.getTurn() * braquage);
		// 2.1) gestion des frottements
		vitesse -= alpha_f;
		vitesse -= beta_f*vitesse;
		// 2.2) gestion de l'acceleration/freinage
		vitesse += c.getAcc() * alpha_c;
		// 2.3) garanties, bornes...
		Vtools.normaliser(direction);
		vitesse = Math.max(0., vitesse); // pas de vitesse négative
		vitesse = Math.min(vmax, vitesse);
		// 3) mise à jour de la position
		position = Vtools.add(position,Vtools.fact(direction, vitesse));
//		System.out.println("Vitesse : "+vitesse);
	}
	public void drive(Commande c) throws VoitureException {
		comPrec=c;
		if (!derapage)
			driveSansDerapage(c);
	}

	public boolean arriveeBonSens(Circuit track) {
		if (Vtools.prodScal(getDirection(),track.getDirectionArrivee())>0){
			return true;
		}
		return false;
	}
//getters
	public double getMaxTurnSansDerapage() {
		/*int i = 0;
		while(i<tabVitesse.length && vitesse<tabVitesse[i]*vmax) i++;
		return tabTurn[i];*/
		
//		int i = 0;
//		for(i=tabVitesse.length-1; i>=0; i--){
//			if(vitesse < tabVitesse[i]*vmax){
//				return tabTurn[i];
//			}
//		}
//		return 1.;
		
		int i = 0;
		while(vitesse>tabVitesse[i]*vmax) i++;
		return tabTurn[i];
	}
	public double getVitesse() {
		return vitesse;
	}
	public Commande getComPrec(){
		return comPrec;
	}
	public Vecteur getPosition() {
		return position;
	}
	public Vecteur getDirection() {
		return direction;
	}
	public boolean getDerapage() {
		return derapage;
	}
	public double getBraquage() {
		return braquage;
	}
	public double getVitesse_sortie_derapage() {
		return vitesse_sortie_derapage;
	}
	public double getVmax() {
		return vmax;
	}
	public double getVpourB(){
		return vitesseMinBraquageMax;
	}
//setters
	public void setVitesse_sortie_derapage(double vitesseSortieDerapage) {
		vitesse_sortie_derapage = vitesseSortieDerapage;
	}
	public void setVitesse(double vitesse) {
		this.vitesse = vitesse;
	}
	public void setDerapage(boolean derapage) {
		this.derapage = derapage;
	}
	public void setPosition(Vecteur position) {
		this.position = position;
	}
	public void setDirection(Vecteur direction) {
		this.direction = direction;
	}
	public Voiture clone(){
		return new VoitureImpl( vmax,  braquage,  alpha_c,
				 alpha_f,  beta_f,  alpha_derapage,
				 vitesse,  Vtools.clonage(position), Vtools.clonage(direction),  vitesse_sortie_derapage);
	}
	

}
